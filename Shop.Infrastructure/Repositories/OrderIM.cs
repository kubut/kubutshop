﻿using System.Collections.Generic;
using Shop.Domain.Model.Order.Repositories;
using NHibernate;
using NHibernate.Cfg;

namespace Shop.Infrastructure.Repositories
{
    public class OrderIM : IOrderRepository
    {
        private ISession session1;

        ISession openSession()
        {
            Configuration cfg = new Configuration();
            cfg.AddAssembly("Shop.Domain");
            cfg.Configure();
            return cfg.BuildSessionFactory().OpenSession();
        }

        public OrderIM()
        {
            session1 = openSession();
        }

        public void Insert(Domain.Model.Order.OrderModel order)
        {
            session1.Save(order);
            session1.Flush();
        }

        public void Update(Domain.Model.Order.OrderModel order)
        {
            session1.Update(order);
        }

        public Domain.Model.Order.OrderModel FindById(int id)
        {
            return session1.Get<Domain.Model.Order.OrderModel>(id);
        }

        // TODO: this is only temp implementation
        public Domain.Model.Order.OrderModel FindBasketByUserId(int id)
        {
            return session1.Get<Domain.Model.Order.OrderModel>(id);
        }

        // TODO: this is only temp implementation
        public IList<Domain.Model.Order.OrderModel> FindByUserId(int userId)
        {
            return
                session1.CreateCriteria<Domain.Model.Order.OrderModel>()
                    .List<Domain.Model.Order.OrderModel>();
        }

        // TODO: this is only temp implementation
        public IList<Domain.Model.Order.OrderModel> FindAllOnProgress()
        {
            return
                session1.CreateCriteria<Domain.Model.Order.OrderModel>()
                    .List<Domain.Model.Order.OrderModel>();
        }

        public IList<Domain.Model.Order.OrderModel> SelectAllOrders()
        {
            return
                session1.CreateCriteria<Domain.Model.Order.OrderModel>()
                    .List<Domain.Model.Order.OrderModel>();
        }
    }
}