﻿using System.Collections.Generic;
using Shop.Domain.Model.Tax;
using Shop.Domain.Model.Tax.Repositories;
using NHibernate;
using NHibernate.Cfg;

namespace Shop.Infrastructure.Repositories
{
    public class TaxIM : ITaxRepository
    {
        private ISession session1;
        static ISession OpenSession()
        {
            Configuration cfg = new Configuration();
            cfg.AddAssembly("Shop.Domain");
            cfg.Configure();
            return cfg.BuildSessionFactory().OpenSession();
        }

        public TaxIM()
        {
            session1 = OpenSession();
        }

        public void Insert(TaxModel tax)
        {
            session1.Save(tax);
            session1.Flush();
        }

        public void Update(TaxModel tax)
        {
            session1.Update(tax);
        }

        public IList<TaxModel> GetList()
        {
            return
                session1.CreateCriteria<TaxModel>()
                    .List<TaxModel>();
        }
    }
}