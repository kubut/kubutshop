﻿using System.Collections.Generic;
using Shop.Domain.Model.User.Repositories;
using NHibernate;
using NHibernate.Cfg;

namespace Shop.Infrastructure.Repositories
{
    public class UserIM : IUserRepository
    {
        static ISession OpenSession()
        {
            Configuration cfg = new Configuration();
            cfg.AddAssembly("Shop.Domain");
            cfg.Configure();
            return cfg.BuildSessionFactory().OpenSession();
        }

        private ISession session1;

        public UserIM()
        {
            session1 = OpenSession();
        }

        public void Insert(Domain.Model.User.UserModel user)
        {
            session1.Save(user);
            session1.Flush();
        }

        public void Update(Domain.Model.User.UserModel user)
        {
            session1.Update(user);
        }

        public void Delete(int id)
        {
            var query = string.Format("delete {0} where id = :id", typeof(Domain.Model.User.UserModel));
            session1.CreateQuery(query).SetParameter("id", id).ExecuteUpdate();
        }

        public Domain.Model.User.UserModel FindUserById(int id)
        {
            return session1.Get<Domain.Model.User.UserModel>(id);
        }

        // TODO: this is only temp implementation
        public Domain.Model.User.UserModel FindUserByName(string name)
        {
            return session1.Get<Domain.Model.User.UserModel>(1);
        }

        public IList<Domain.Model.User.UserModel> GetUserList()
        {
            return
                session1.CreateCriteria<Domain.Model.User.UserModel>()
                    .List<Domain.Model.User.UserModel>();
        }
    }
}