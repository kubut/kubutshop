﻿using System.Collections.Generic;
using Shop.Domain.Model.ProductCategory.Repositories;
using NHibernate;
using NHibernate.Cfg;

namespace Shop.Infrastructure.Repositories
{
     public class ProductCategoryIM : IProductCategoryRepository
     {
         private ISession session1;
         static ISession OpenSession()
         {
             Configuration cfg = new Configuration();
             cfg.AddAssembly("Shop.Domain");
             cfg.Configure();
             return cfg.BuildSessionFactory().OpenSession();
         }

         public ProductCategoryIM()
         {
             session1 = OpenSession();
         }

        public void Insert(Domain.Model.ProductCategory.ProductCategoryModel cat)
        {
            session1.Save(cat);
            session1.Flush();
        }

        public void Update(Domain.Model.ProductCategory.ProductCategoryModel cat)
        {
            session1.Update(cat);
        }

        public void Delete(int id)
        {
            var query = string.Format("delete {0} where id = :id", typeof(Domain.Model.ProductCategory.ProductCategoryModel));
            session1.CreateQuery(query).SetParameter("id", id).ExecuteUpdate();
        }

        public IList<Domain.Model.ProductCategory.ProductCategoryModel> GetList()
        {
            return
                session1.CreateCriteria<Domain.Model.ProductCategory.ProductCategoryModel>()
                    .List<Domain.Model.ProductCategory.ProductCategoryModel>();
        }

         // TODO: this is only temp implementation
        public IList<Domain.Model.ProductCategory.ProductCategoryModel> GetSubList(Domain.Model.ProductCategory.ProductCategoryModel cat)
        {
            return
                session1.CreateCriteria<Domain.Model.ProductCategory.ProductCategoryModel>()
                    .List<Domain.Model.ProductCategory.ProductCategoryModel>();
        }

        public Domain.Model.ProductCategory.ProductCategoryModel GetCategoryById(int id)
        {
            return session1.Load<Domain.Model.ProductCategory.ProductCategoryModel>(id);
        }
    }
}
