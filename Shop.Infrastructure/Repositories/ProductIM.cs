﻿using System.Collections.Generic;
using Shop.Domain.Model.Product.Repositories;
using NHibernate;
using NHibernate.Cfg;
using Shop.Domain.Model.Product;

namespace Shop.Infrastructure.Repositories
{
    public class ProductIM : IProductRepository
    {
        private ISession session1;

        private ISession openSession()
        {
            Configuration cfg = new Configuration();
            cfg.AddAssembly("Shop.Domain");
            cfg.Configure();
            return cfg.BuildSessionFactory().OpenSession();
        }

        public ProductIM()
        {
            session1 = openSession();
        }

        public void Insert(Domain.Model.Product.ProductModel product)
        {
            session1.Save(product);
            session1.Flush();
        }

        public void Update(Domain.Model.Product.ProductModel product)
        {
            session1.Update(product);
        }

        public void Delete(int id)
        {
            var query = string.Format("delete {0} where id = :id", typeof(Domain.Model.Product.ProductModel));
            session1.CreateQuery(query).SetParameter("id", id).ExecuteUpdate();
        }

        public IList<Domain.Model.Product.ProductModel> GetList()
        {
            return
                session1.CreateCriteria<ProductModel>()
                    .List<ProductModel>();
        }
        // TODO: this is only temmp implementation
        public IList<Domain.Model.Product.ProductModel> GetListFromCategory(Domain.Model.ProductCategory.ProductCategoryModel cat)
        {
            return
                session1.CreateCriteria<ProductModel>()
                    .List<ProductModel>();
        }

        public Domain.Model.Product.ProductModel GetProductById(int id)
        {
            return session1.Get<ProductModel>(id);
        }
    }
}
