﻿using System.Collections.Generic;
using Shop.Domain.Model.Producer.Repositories;
using NHibernate;
using NHibernate.Cfg;

namespace Shop.Infrastructure.Repositories
{
    public class ProducerIM : IProducerRepository
    {
        static ISession OpenSession()
        {
            Configuration cfg = new Configuration();
            cfg.AddAssembly("Shop.Domain");
            cfg.Configure();
            return cfg.BuildSessionFactory().OpenSession();
        }

        private ISession session1;

        public ProducerIM()
        {
            session1 = OpenSession();
        }

        public void Insert(Domain.Model.Producer.ProducerModel producer)
        {
            session1.Save(producer);
            session1.Flush();
        }

        public void Delete(int id)
        {
            var query = string.Format("delete {0} where id = :id", typeof (Domain.Model.Producer.ProducerModel));
            session1.CreateQuery(query).SetParameter("id", id).ExecuteUpdate();
        }

        public IList<Domain.Model.Producer.ProducerModel> GetList()
        {
            return
                session1.CreateCriteria<Domain.Model.Producer.ProducerModel>()
                    .List<Domain.Model.Producer.ProducerModel>();
        }
    }
}