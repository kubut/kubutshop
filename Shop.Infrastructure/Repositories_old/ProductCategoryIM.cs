﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Domain.Model.ProductCategory;
using Shop.Domain.Model.ProductCategory.Repositories;

namespace Shop.Infrastructure.Repositories_old
{
    public class ProductCategoryIM : IProductCategoryRepository
    {
        private List<ProductCategoryModel> categories = new List<ProductCategoryModel>();

        public ProductCategoryIM()
        {
            List<ProductCategoryModel> subCategories = new List<ProductCategoryModel>();

            subCategories = new List<ProductCategoryModel>
            {
                new ProductCategoryModel {id = 3, name = "Myszki", parentId = 2, subCategory = null},
                new ProductCategoryModel {id = 4, name = "Klawiatury", parentId = 2, subCategory = null}
            };

            categories = new List<ProductCategoryModel>
            {
                new ProductCategoryModel {id = 1, name = "Monitory", parentId = 0, subCategory = null},
                new ProductCategoryModel {id = 2, name = "Peryferia", parentId = 0, subCategory = subCategories},
                new ProductCategoryModel {id = 5, name = "Procesory", parentId = 0, subCategory = null}
            };
        }

        public void Insert(ProductCategoryModel cat)
        {
            categories.Add(cat);
        }

        public void Delete(int id)
        {
            foreach (var p in categories)
                if (p.id == id)
                    categories.Remove(p);
        }

        public void Update(ProductCategoryModel cat)
        {
            for (int i = 0; i < categories.Count; i++)
            {
                if (categories[i].id == cat.id)
                {
                    categories[i] = cat;
                }
            }
        }

        public List<ProductCategoryModel> GetList()
        {
            return categories;
        }

        public List<ProductCategoryModel> GetSubList(ProductCategoryModel cat)
        {
            foreach (var p in categories)
                if (p.id == cat.id)
                    return p.subCategory;
            return null;
        }

        public ProductCategoryModel GetCategoryById(int id)
        {
            foreach (var p in categories)
                if (p.id == id)
                    return p;
            return null;
        }
    }
}
