﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Domain.Model.Producer;
using Shop.Domain.Model.Producer.Repositories;

namespace Shop.Infrastructure.Repositories_old
{
    public class ProducerIM_old : IProducerRepository
    {
        private List<ProducerModel> producers = new List<ProducerModel>();

        public ProducerIM_old()
        {
            producers = new List<ProducerModel>{
                new ProducerModel {id = 1, name = "Asus"},
                new ProducerModel {id = 2, name = "Lenovo"},
                new ProducerModel {id = 3, name = "Gigabyte"},
                new ProducerModel {id = 4, name = "Intel"}
            };
        }

        public void Insert(ProducerModel producer)
        {
            producers.Add(producer);
        }

        public void Delete(int id)
        {
            foreach (var p in producers)
                if (p.id == id)
                    producers.Remove(p);
        }

        public IList<ProducerModel> GetList()
        {
            return producers;
        }
    }
}
