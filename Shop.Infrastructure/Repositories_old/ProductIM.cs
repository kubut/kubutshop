﻿using System;
using System.Collections.Generic;
using Shop.Domain.Model.Product;
using Shop.Domain.Model.Product.Repositories;
using Shop.Domain.Model.ProductCategory;
using Shop.Domain.Model.ValueObject;
using Shop.Domain.Model.Tax;

namespace Shop.Infrastructure.Repositories_old
{
    public class ProductIM : IProductRepository
    {
        private List<ProductModel> products = new List<ProductModel>();
        private ProducerIM_old producerIM = new ProducerIM_old();
        private ProductCategoryIM productCategoryIM = new ProductCategoryIM();

        public ProductIM()
        {
            products = new List<ProductModel>
            {
                new ProductModel {
                    id = 1,
                    name = "klawiatura 1",
                    price = new Money {currency = Money.Currency.pln, value = 300},
                    discount = 10,
                    tax = new TaxModel {id = 1, tax = 23},
                    desc = "",
                    avaible = 1,
                    date = new DateTime(),
                    producer = producerIM.GetList()[0],
                    category = productCategoryIM.GetCategoryById(4)
                },
                new ProductModel {
                    id = 2,
                    name = "mysz 1",
                    price = new Money {currency = Money.Currency.pln, value = 300},
                    discount = 0,
                    tax = new TaxModel {id = 1, tax = 23},
                    desc = "",
                    avaible = 1,
                    date = new DateTime(),
                    producer = producerIM.GetList()[0],
                    category = productCategoryIM.GetCategoryById(3)
                },
                new ProductModel {
                    id = 3,
                    name = "Celeron",
                    price = new Money {currency = Money.Currency.pln, value = 300},
                    discount = 0,
                    tax = new TaxModel {id = 1, tax = 23},
                    desc = "",
                    avaible = 1,
                    date = new DateTime(),
                    producer = producerIM.GetList()[0],
                    category = productCategoryIM.GetCategoryById(5)
                },
                new ProductModel {
                    id = 4,
                    name = "Monitor",
                    price = new Money {currency = Money.Currency.pln, value = 300},
                    discount = 0,
                    tax = new TaxModel {id = 1, tax = 23},
                    desc = "",
                    avaible = 1,
                    date = new DateTime(),
                    producer = producerIM.GetList()[0],
                    category = productCategoryIM.GetCategoryById(1)
                }
            };
        }

        public void Insert(ProductModel product)
        {
            products.Add(product);
        }

        public void Delete(int id)
        {
            foreach (var p in products)
                if (p.id == id)
                    products.Remove(p);
        }

        public void Update(ProductModel product)
        {
            for (int i = 0; i < products.Count; i++)
            {
                if (products[i].id == product.id)
                {
                    products[i] = product;
                }
            }
        }

        public IList<ProductModel> GetList()
        {
            return products;
        }

        public IList<ProductModel> GetListFromCategory(ProductCategoryModel cat)
        {
            List<ProductModel> returnList = new List<ProductModel>();
            foreach (var p in products)
                if (p.category.id == cat.id)
                    returnList.Add(p);
            return returnList;
        }

        public ProductModel GetProductById(int id)
        {
            foreach (var p in products)
                if (p.id == id)
                    return p;
            return null;
        }
    }
}
