﻿using System;
using System.Collections.Generic;
using Shop.Domain.Model.Order;
using Shop.Domain.Model.Order.Repositories;
using Shop.Domain.Model.User;
using Shop.Domain.Model.ValueObject;

namespace Shop.Infrastructure.Repositories_old
{
    public class OrderIM : IOrderRepository
    {
        private List<OrderModel> orders = new List<OrderModel>();
        private UserModel user1 = new UserModel { id = 1, adress = new Address { } };
        private UserModel user2 = new UserModel { id = 2, adress = new Address { } };
        public OrderIM()
        {
            orders = new List<OrderModel>
            {
                new OrderModel {
                    id = 1,
                    user = user1,
                    address = user1.adress,
                    productList = new List<OrderProductModel>() {
                        new OrderProductModel {id = 1},
                        new OrderProductModel {id = 2}
                    },
                    paymentMethod = new PaymentType {method = PaymentType.PaymentMethod.card},
                    status = new PaymentStatus { status = PaymentStatus.Status.waitForPay},
                    paymentDate = new DateTime(),
                    finish = false
                },
                new OrderModel {
                    id = 2,
                    user = user1,
                    address = user1.adress,
                    productList = new List<OrderProductModel>() {
                        new OrderProductModel {id = 3},
                        new OrderProductModel {id = 4}
                    },
                    paymentMethod = new PaymentType {method = PaymentType.PaymentMethod.card},
                    status = new PaymentStatus { status = PaymentStatus.Status.send},
                    paymentDate = new DateTime(),
                    finish = true
                },
                new OrderModel {
                    id = 3,
                    user = user2,
                    address = user2.adress,
                    productList = new List<OrderProductModel>() {
                        new OrderProductModel {id = 5}
                    },
                    paymentMethod = new PaymentType {method = PaymentType.PaymentMethod.card},
                    status = new PaymentStatus { status = PaymentStatus.Status.waitForPay},
                    paymentDate = new DateTime(),
                    finish = false
                }
            };
        }

        public void Insert(OrderModel order)
        {
            orders.Add(order);
        }

        public void Update(OrderModel order)
        {
            for (int i = 0; i < orders.Count; i++)
            {
                if (orders[i].id == order.id)
                {
                    orders[i] = order;
                }
            }
        }

        public OrderModel FindById(int id)
        {
            foreach (var p in orders)
                if (p.id == id)
                    return p;
            return null;
        }

        public OrderModel FindBasketByUserId(int id)
        {
            foreach (var p in orders)
                if (p.user.id == id  && p.status.status == PaymentStatus.Status.basket)
                    return p;
            return null;
        }

        public List<OrderModel> FindByUserId(int userId)
        {
            List<OrderModel> returnList = new List<OrderModel>();
            foreach (var p in orders)
                if (p.user.id == userId)
                    returnList.Add(p);
            return returnList;
        }

        public List<OrderModel> FindAllOnProgress()
        {
            List<OrderModel> returnList = new List<OrderModel>();
            foreach (var p in orders)
                if (p.finish)
                    returnList.Add(p);
            return returnList;
        }

        public List<OrderModel> SelectAllOrders()
        {
            return orders;
        }


        public Money GetOrderValue(int id)
        {
            foreach (var p in orders)
            {
                if (p.id == id)
                {
                    int value = 0;
                    foreach (var pp in p.productList)
                    {
                        value += pp.price.value;
                    }
                    return new Money { currency = p.productList[0].price.currency, value = value };
                }
            }
            return null;

        }
    }
}
