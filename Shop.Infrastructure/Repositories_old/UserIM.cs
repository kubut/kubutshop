﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Domain.Model.User;
using Shop.Domain.Model.User.Repositories;
using Shop.Domain.Model.ValueObject;
using Shop.Domain.Model.Order;

namespace Shop.Infrastructure.Repositories_old
{
    public class UserIM : IUserRepository
    {
        private List<UserModel> users = new List<UserModel>();
        private OrderIM orderIM = new OrderIM();

        public UserIM()
        {
            users = new List<UserModel>
            {
                new UserModel{
                    id = 1,
                    username = "User1",
                    adress = new Address {},
                    password = "hasz",
                    discount = 0,
                    userType = new UserType{ type = UserType.Type.customer},
                    orderHistory = orderIM.FindByUserId(1)
                },
                new UserModel{
                    id = 2,
                    username = "User2",
                    adress = new Address {},
                    password = "hasz",
                    discount = 0,
                    userType = new UserType{ type = UserType.Type.customer},
                    orderHistory = orderIM.FindByUserId(2)
                },
                new UserModel{
                    id = 3,
                    username = "User3",
                    adress = new Address {},
                    password = "hasz",
                    discount = 0,
                    userType = new UserType{ type = UserType.Type.customer},
                    orderHistory = orderIM.FindByUserId(3)
                },
                new UserModel{
                    id = 4,
                    username = "Admin",
                    adress = new Address {},
                    password = "hasz",
                    discount = 0,
                    userType = new UserType{ type = UserType.Type.owner},
                    orderHistory = null
                },
            };
        }

        public void Insert(UserModel user)
        {
            users.Add(user);
        }

        public void Delete(int id)
        {
            foreach (var p in users)
                if (p.id == id)
                    users.Remove(p);
        }

        public void Update(UserModel user)
        {
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].id == user.id)
                {
                    users[i] = user;
                }
            }
        }

        public UserModel FindUserById(int id)
        {
            foreach (var p in users)
                if (p.id == id)
                    return p;
            return null;
        }

        public UserModel FindUserByName(string name)
        {
            foreach (var p in users)
                if (p.username == name)
                    return p;
            return null;
        }

        public List<UserModel> GetUserList()
        {
            return users;
        }
    }
}
