﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Domain.Model.Tax;

namespace Shop.Infrastructure.Repositories_old
{
    public class TaxIM
    {
        private List<TaxModel> taxes = new List<TaxModel>();

        public TaxIM()
        {
            taxes = new List<TaxModel>
            {
                new TaxModel {id = 1, tax = 23},
                new TaxModel {id = 2, tax = 8}
            };
        }

        public void Insert(TaxModel tax)
        {
            taxes.Add(tax);
        }

        public void Update(TaxModel tax)
        {
            for (int i = 0; i < taxes.Count; i++)
            {
                if (taxes[i].id == tax.id)
                {
                    taxes[i] = tax;
                }
            }
        }

        public List<TaxModel> GetList()
        {
            return taxes;
        }
    }
}
