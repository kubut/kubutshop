﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Domain.Model.Product;
using Shop.Domain.Model.ProductCategory;

namespace Shop.Domain.Model.Product.Repositories
{
    public interface IProductRepository
    {
        void Insert(ProductModel product);
        void Update(ProductModel product);
        void Delete(int id);

        IList<ProductModel> GetList();
        IList<ProductModel> GetListFromCategory(ProductCategoryModel cat);

        ProductModel GetProductById(int id);
    }
}
