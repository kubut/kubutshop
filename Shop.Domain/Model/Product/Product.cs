﻿using System;
using Shop.Domain.Model.ValueObject;
using Shop.Domain.Model.ProductCategory;
using Shop.Domain.Model.Producer;
using Shop.Domain.Model.Tax;

namespace Shop.Domain.Model.Product
{
    public class ProductModel
    {
        public virtual int id { get; set; }
        public virtual string name { get; set; }

        public virtual Money price { get; set; }
        public virtual int discount { get; set; }
        public virtual TaxModel tax { get; set; }
        public virtual string desc { get; set; }
        public virtual int avaible { get; set; }
        public virtual DateTime date { get; set; }
        public virtual ProducerModel producer { get; set; }
        public virtual ProductCategoryModel category { get; set; }
        public virtual Money priceVat
        {
            get { return new Money { currency = price.currency, value = price.value * ((float)tax.tax / 100) }; }
        }

        public virtual Money priceNetto
        {
            get { return new Money { currency = price.currency, value = price.value - priceVat.value }; }
        }
    }
}
/*
create table ProductModel (
	id int primary key identity,
	name varchar(60),
	value int,
	currency varchar(5),
	discount int,
	tax int references TaxModel(id),
	descript text,
	avaible int,
	date date,
	producer int references ProducerModel(id),
	category int references ProductCategoryModel(id)
)
*/