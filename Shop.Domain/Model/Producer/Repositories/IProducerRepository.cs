﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Domain.Model.Producer.Repositories
{
    public interface IProducerRepository
    {
        void Insert(ProducerModel product);
        void Delete(int id);

        IList<ProducerModel> GetList();
    }
}
