﻿namespace Shop.Domain.Model.Producer
{
    public class ProducerModel
    {
        public virtual int id { get; set; }
        public virtual string name { get; set; }
    }
}
/*
CREATE TABLE [dbo].[ProducerModel] (
    [id]   INT          IDENTITY (1, 1) NOT NULL,
    [name] VARCHAR (40) NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);
*/