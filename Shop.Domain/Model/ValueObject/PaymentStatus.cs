﻿namespace Shop.Domain.Model.ValueObject
{
    public enum Status : int { basket = 0, waiting = 1, send = 2, waitForPay = 3 }
}
