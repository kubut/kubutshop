﻿namespace Shop.Domain.Model.ValueObject
{
    public class Address
    {
        public virtual string FirstName { get; set; }
        public virtual string SecondName { get; set; }
        public virtual string Address1 { get; set; }
        public virtual string Address2 { get; set; }
        public virtual string Phone { get; set; }
    }
}
