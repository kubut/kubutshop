﻿namespace Shop.Domain.Model.ValueObject
{
    public enum PaymentMethod : int { card = 1, online = 2, cash = 3 }
}
