﻿using System;

namespace Shop.Domain.Model.ValueObject
{
    public enum Currency { pln = 1, euro = 2 }

    public class Money
    {
        public virtual Currency currency { get; set; }
        public virtual double value { get; set; }
        public static Money operator +(Money m1, Money m2)
        {
            if(m1.currency == m2.currency){
                Money sum = new Money();
                sum.currency = m1.currency;
                sum.value = m1.value + m2.value;
                return sum;
            } else {
                throw new ArgumentException();
            }
        }
    }
}
