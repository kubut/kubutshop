﻿using System;
using System.Collections.Generic;
using Iesi.Collections.Generic;
using Shop.Domain.Model.User;
using Shop.Domain.Model.ValueObject;

namespace Shop.Domain.Model.Order
{
    public class OrderModel
    {
        public OrderModel() { productList = new List<OrderProductModel>();}
        public virtual int id { get; set; }
        public virtual int finish { get; set; }
        public virtual UserModel user { get; set; }
        public virtual Address address { get; set; }
        public virtual IList<OrderProductModel> productList { get; set; }
        public virtual PaymentMethod paymentMethod { get; set; }
        public virtual Status status { get; set; }
        public virtual DateTime paymentDate { get; set; }
        public virtual Money orderValue { get; set; }
        public virtual int productsCount { get; set; }
    }
}
/*
create table OrderModel (
	id int primary key identity,
	finish int,
	userId int references UserModel(id),
	FirstName varchar(100),
	SecondName varchar(100),
	Address1 varchar(100),
	Address2 varchar(100),
	Phone varchar(15),
	paymentMethod varchar(15),
	paymentStatus varchar(15),
	paymentDate date
)
*/