﻿using System.Collections.Generic;
namespace Shop.Domain.Model.Order.Repositories
{
    public interface IOrderRepository
    {
        void Insert(OrderModel order);
        void Update(OrderModel order);

        OrderModel FindById(int id);
        OrderModel FindBasketByUserId(int id);
        IList<OrderModel> FindByUserId(int userId);
        IList<OrderModel> FindAllOnProgress();
        IList<OrderModel> SelectAllOrders();
    }
}
