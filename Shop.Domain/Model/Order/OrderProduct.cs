﻿using Shop.Domain.Model.ValueObject;
using Shop.Domain.Model.Tax;


namespace Shop.Domain.Model.Order
{
    public class OrderProductModel
    {
        public virtual int id { get; set; }
        public virtual string name { get; set; }

        public virtual OrderModel orderM { get; set; }
        public virtual Money price { get; set; }

        public virtual Money priceVat
        {
            get { return new Money { currency = price.currency, value = price.value * ((float) tax.tax / 100) }; }
        }

        public virtual Money priceNetto
        {
            get { return new Money {currency = price.currency, value = price.value - priceVat.value}; }
        }

        public virtual TaxModel tax { get; set; }
        public virtual int count { get; set; }
    }
}
/*
create table OrderProductModel (
	id int primary key identity,
	name varchar(60),
	value int,
	currency varchar(5),
	countNumber int,
	tax int references TaxModel(id),
    orderM int references OrderModel(id)
)
*/