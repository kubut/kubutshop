﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Domain.Model.Tax.Repositories
{
    public interface ITaxRepository
    {
        void Insert(TaxModel tax);
        void Update(TaxModel tax);

        IList<TaxModel> GetList();
    }
}
