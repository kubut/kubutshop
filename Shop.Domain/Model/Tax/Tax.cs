﻿namespace Shop.Domain.Model.Tax
{
    public class TaxModel
    {
        public virtual int id { get; set; }
        public virtual int tax { get; set; }
    }
}
/*
CREATE TABLE [dbo].[TaxModel] (
    [id]  INT IDENTITY (1, 1) NOT NULL,
    [tax] INT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);
*/