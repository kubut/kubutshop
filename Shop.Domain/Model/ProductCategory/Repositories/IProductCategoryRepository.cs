﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Domain.Model.ProductCategory;

namespace Shop.Domain.Model.ProductCategory.Repositories
{
    public interface IProductCategoryRepository
    {
        void Insert(ProductCategoryModel cat);
        void Update(ProductCategoryModel cat);
        void Delete(int id);

        IList<ProductCategoryModel> GetList();
        IList<ProductCategoryModel> GetSubList(ProductCategoryModel cat);
        ProductCategoryModel GetCategoryById(int id);
    }
}
