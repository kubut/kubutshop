﻿namespace Shop.Domain.Model.ProductCategory
{
    public class ProductCategoryModel
    {
        public ProductCategoryModel() { subCategory = new Iesi.Collections.Generic.HashedSet<ProductCategoryModel>(); }
        public virtual int id { get; set; }
        public virtual string name { get; set; }
        public virtual ProductCategoryModel parentId { get; set; }
        public virtual Iesi.Collections.Generic.ISet<ProductCategoryModel> subCategory { get; set; }
    }
}
/*
create table ProductCategoryModel
(
	id int primary key identity,
	name varchar(50),
	parentId int references ProductCategoryModel(id)
)
go
*/