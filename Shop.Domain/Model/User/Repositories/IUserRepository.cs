﻿using System.Collections.Generic;

namespace Shop.Domain.Model.User.Repositories
{
    public interface IUserRepository
    {
        void Insert(UserModel user);
        void Update(UserModel user);
        void Delete(int id);

        UserModel FindUserById(int id);
        UserModel FindUserByName(string name);
        IList<UserModel> GetUserList();
    }
}
