﻿using System.Collections.Generic;
using Shop.Domain.Model.ValueObject;
using Shop.Domain.Model.Order;

namespace Shop.Domain.Model.User
{
    public class UserModel
    {

        public virtual int id { get; set; }
        public virtual string username { get; set; }

        public virtual Address adress { get; set; }
        public virtual string password { get; set; }
        public virtual int discount { get; set; }
        public virtual Type userType { get; set; }

        public virtual ISet<OrderModel> orderHistory { get; set; }
    }
}
/*
create table UserModel
(
	id int primary key identity,
	username varchar(40),
	FirstName varchar(100),
	SecondName varchar(100),
	Address1 varchar(100),
	Address2 varchar(100),
	Phone varchar(15),
	password varchar(64),
	discount int,
	userType varchar(30),
)
go
*/