﻿using Shop.Application;
using System;
using System.Collections.Generic;
using Shop.Application.Front;
using Shop.Application.Admin;
using Shop.Domain.Model.Order;
using Shop.Domain.Model.Product;
using Shop.Domain.Model.ProductCategory;
using Shop.Domain.Model.User;
using Shop.Domain.Model.ValueObject;
using Type = Shop.Domain.Model.ValueObject.Type;

namespace Presentation.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //InitData.init();
            Console.ReadKey();
        }
    }
}
