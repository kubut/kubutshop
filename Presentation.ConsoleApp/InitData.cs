﻿using System;
using System.Collections.Generic;
using Iesi.Collections.Generic;
using NHibernate;
using NHibernate.Cfg;
using Shop.Domain.Model.Producer;
using Shop.Domain.Model.Order;
using Shop.Domain.Model.Product;
using Shop.Domain.Model.ProductCategory;
using Shop.Domain.Model.Tax;
using Shop.Domain.Model.User;
using Shop.Domain.Model.ValueObject;
using Type = Shop.Domain.Model.ValueObject.Type;

namespace Presentation.ConsoleApp
{
    public class InitData
    {
        public static void init()
        {
            Configuration cfg = new Configuration();
            cfg.AddAssembly("Shop.Domain");
            cfg.Configure();
            IStatelessSession session = cfg.BuildSessionFactory().OpenStatelessSession();

            // =============================== TAX ==========================================
            
            var tax23 = new TaxModel {tax = 23};
            var tax8 = new TaxModel { tax = 8 };
            session.Insert(tax23);
            session.Insert(tax8);
            Console.WriteLine("Dodano stawki podatkow");
            

            // =============================== CATEGORY ==========================================
            
            var catMonitory = new ProductCategoryModel { name = "Monitory"};
            var catMonitoryLCD = new ProductCategoryModel { name = "LCD", parentId = catMonitory };
            var catMonitoryCRT = new ProductCategoryModel { name = "CRT", parentId = catMonitory };
            var catProcesory = new ProductCategoryModel { name = "Procesory" };
            var catProcesoryIntel = new ProductCategoryModel { name = "Intel", parentId = catProcesory };
            var catProcesoryAmd = new ProductCategoryModel { name = "AMD", parentId = catProcesory };
            session.Insert(catMonitory);
            session.Insert(catProcesory);

            session.Insert(catProcesoryIntel);
            session.Insert(catProcesoryAmd);

            session.Insert(catMonitoryCRT);
            session.Insert(catMonitoryLCD);
            Console.WriteLine("Dodano kategorie produktow");
            

            // =============================== USER ==========================================
            Address a1 = new Address
            {
                Address1 = "Ul. Jerzego",
                Address2 = "Wroclaw",
                FirstName = "Jerzy",
                SecondName = "Iksinski",
                Phone = "123456789"
            };
            Address a2 = new Address
            {
                Address1 = "Ul. Jerzego",
                Address2 = "Wroclaw",
                FirstName = "Jerzy",
                SecondName = "Iksinski",
                Phone = "123456789"
            };
            Address a3 = new Address
            {
                Address1 = "Ul. Jerzego",
                Address2 = "Wroclaw",
                FirstName = "Jerzy",
                SecondName = "Iksinski",
                Phone = "123456789"
            };
            Address a4 = new Address
            {
                Address1 = "Ul. Jerzego",
                Address2 = "Wroclaw",
                FirstName = "Jerzy",
                SecondName = "Iksinski",
                Phone = "123456789"
            };

            UserModel user1 = new UserModel
            {
                adress = a1,
                discount = 0,
                username = "iks",
                password = "dddd",
                userType = Type.customer
            };
            UserModel user2 = new UserModel
            {
                adress = a2,
                discount = 10,
                username = "zenek",
                password = "dddd",
                userType = Type.customer
            };
            UserModel worker = new UserModel
            {
                adress = a3,
                discount = 0,
                username = "Krysia",
                password = "dddd",
                userType = Type.worker
            };
            UserModel owner = new UserModel
            {
                adress = a4,
                discount = 0,
                username = "kubut",
                password = "dddd",
                userType = Type.owner
            };
            session.Insert(user1);
            session.Insert(user2);
            session.Insert(worker);
            session.Insert(owner);
            Console.WriteLine("Dodano uzytkownikow");

            // =============================== ORDER ==========================================
            OrderModel order1 = new OrderModel
            {
                address = user1.adress,
                finish = 0,
                paymentMethod = PaymentMethod.cash,
                status = Status.waitForPay,
                user = user1,
                paymentDate = new DateTime(2015,10,2)
            };
            OrderModel order2 = new OrderModel
            {
                address = user2.adress,
                finish = 1,
                paymentMethod = PaymentMethod.cash,
                status = Status.send,
                user = user2,
                paymentDate = new DateTime(2015, 10, 2)

            };
            OrderModel order3 = new OrderModel
            {
                address = user2.adress,
                finish = 0,
                paymentMethod = PaymentMethod.online,
                status = Status.waitForPay,
                user = user2,
                paymentDate = new DateTime(2015, 10, 2)

            };
            OrderModel basket1 = new OrderModel
            {
                address = user1.adress,
                finish = 0,
                status = Status.basket,
                user = user1,
                paymentDate = new DateTime(2015, 10, 2)
            };
            OrderModel basket2 = new OrderModel
            {
                address = user2.adress,
                finish = 0,
                status = Status.basket,
                user = user2,
                paymentDate = new DateTime(2015, 10, 2)
            };

            OrderProductModel orderProduct1 = new OrderProductModel
            {
                count = 2,
                name = "Lenovo Y510p",
                orderM = order1,
                price = new Money {currency = Currency.pln, value = 400000},
                tax = tax23
            };
            OrderProductModel orderProduct11 = new OrderProductModel
            {
                count = 2,
                name = "Myszka",
                orderM = order1,
                price = new Money {currency = Currency.pln, value = 400},
                tax = tax23
            };
            OrderProductModel orderProduct2 = new OrderProductModel
            {
                count = 1,
                name = "Procesor",
                orderM = order2,
                price = new Money { currency = Currency.pln, value = 50000 },
                tax = tax23
            };
            OrderProductModel orderProduct22 = new OrderProductModel
            {
                count = 1,
                name = "Plyta glowna",
                orderM = order2,
                price = new Money { currency = Currency.pln, value = 70000 },
                tax = tax23
            };
            OrderProductModel orderProduct3 = new OrderProductModel
            {
                count = 1,
                name = "Plyta cd",
                orderM = order3,
                price = new Money { currency = Currency.pln, value = 300 },
                tax = tax23
            };
            order1.productList = new List<OrderProductModel>
            {
                orderProduct1,
                orderProduct11
            };
            order2.productList = new List<OrderProductModel>
            {
                orderProduct2,
                orderProduct22
            };
            order3.productList = new List<OrderProductModel>
            {
                orderProduct3
            };
            session.Insert(order1);
            session.Insert(order2);
            session.Insert(order3);
            session.Insert(basket1);
            session.Insert(basket2);
            
            session.Insert(orderProduct1);
            session.Insert(orderProduct11);
            session.Insert(orderProduct2);
            session.Insert(orderProduct22);
            session.Insert(orderProduct3);
            
            Console.WriteLine("Dodano zamowienia");

            // =============================== PRODUCER ==========================================
            ProducerModel producer1 = new ProducerModel { name = "Intel" };
            ProducerModel producer2 = new ProducerModel { name = "AMD" };
            ProducerModel producer3 = new ProducerModel { name = "Samsung" };
            session.Insert(producer1);
            session.Insert(producer2);
            session.Insert(producer3);
            Console.WriteLine("Dodano producentow");

            // =============================== PRODUCT ==========================================
            ProductModel product1 = new ProductModel
            {
                avaible = 1,
                category = catMonitoryLCD,
                date = new DateTime(2015, 10, 12),
                desc = "Jakis opis",
                discount = 0,
                name = "Samsung LCD",
                price = new Money {currency = Currency.pln, value = 3000},
                producer = producer3,
                tax = tax23
            };
            ProductModel product2 = new ProductModel
            {
                avaible = 1,
                category = catMonitoryCRT,
                date = new DateTime(2013, 10, 12),
                desc = "Jakis opis",
                discount = 0,
                name = "Samsung CRT",
                price = new Money { currency = Currency.pln, value = 3000 },
                producer = producer3,
                tax = tax23
            };
            ProductModel product3 = new ProductModel
            {
                avaible = 1,
                category = catMonitoryLCD,
                date = new DateTime(2012, 10, 12),
                desc = "Jakis opis",
                discount = 0,
                name = "Samsung LCD2",
                price = new Money { currency = Currency.pln, value = 3000 },
                producer = producer3,
                tax = tax23
            };
            ProductModel product4 = new ProductModel
            {
                avaible = 1,
                category = catProcesoryIntel,
                date = new DateTime(2015, 10, 12),
                desc = "Jakis opis",
                discount = 0,
                name = "Intel szybki procesor",
                price = new Money { currency = Currency.pln, value = 3000 },
                producer = producer1,
                tax = tax23
            };
            ProductModel product5 = new ProductModel
            {
                avaible = 1,
                category = catProcesoryIntel,
                date = new DateTime(2013, 10, 12),
                desc = "Jakis opis",
                discount = 0,
                name = "Intel wolny...",
                price = new Money { currency = Currency.pln, value = 3000 },
                producer = producer1,
                tax = tax23
            };
            ProductModel product6 = new ProductModel
            {
                avaible = 1,
                category = catProcesoryAmd,
                date = new DateTime(2013, 10, 12),
                desc = "Jakis opis",
                discount = 0,
                name = "Amd szybki...",
                price = new Money { currency = Currency.pln, value = 3000 },
                producer = producer2,
                tax = tax23
            };
            ProductModel product7 = new ProductModel
            {
                avaible = 0,
                category = catProcesoryAmd,
                date = new DateTime(2013, 10, 12),
                desc = "Jakis opis",
                discount = 0,
                name = "Amd wolny niedostepny...",
                price = new Money { currency = Currency.pln, value = 3000 },
                producer = producer2,
                tax = tax23
            };
            session.Insert(product1);
            session.Insert(product2);
            session.Insert(product3);
            session.Insert(product4);
            session.Insert(product5);
            session.Insert(product6);
            session.Insert(product7);
            Console.WriteLine("Dodano produkty");
        } 
    }
}
/*

CREATE TABLE [dbo].[TaxModel] (
    [id]  INT IDENTITY (1, 1) NOT NULL,
    [tax] INT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);
go

create table ProductCategoryModel
(
	id int primary key identity,
	name varchar(50),
	parentId int references ProductCategoryModel(id)
)
go

CREATE TABLE [dbo].[ProducerModel] (
    [id]   INT          IDENTITY (1, 1) NOT NULL,
    [name] VARCHAR (40) NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);
go

create table UserModel
(
	id int primary key identity,
	username varchar(40),
	FirstName varchar(100),
	SecondName varchar(100),
	Address1 varchar(100),
	Address2 varchar(100),
	Phone varchar(15),
	password varchar(64),
	discount int,
	userType varchar(30),
)
go

create table ProductModel (
	id int primary key identity,
	name varchar(60),
	value int,
	currency varchar(5),
	discount int,
	tax int references TaxModel(id),
	descript text,
	avaible int,
	date date,
	producer int references ProducerModel(id),
	category int references ProductCategoryModel(id)
)
go

create table OrderModel (
	id int primary key identity,
	finish int,
	userId int references UserModel(id),
	FirstName varchar(100),
	SecondName varchar(100),
	Address1 varchar(100),
	Address2 varchar(100),
	Phone varchar(15),
	paymentMethod varchar(15),
	paymentStatus varchar(15),
	paymentDate date
)
go

create table OrderProductModel (
	id int primary key identity,
	name varchar(60),
	value int,
	currency varchar(5),
	countNumber int,
	tax int references TaxModel(id),
    orderM int references OrderModel(id)
)
go


*/