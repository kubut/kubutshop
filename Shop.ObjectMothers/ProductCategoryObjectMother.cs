﻿using Iesi.Collections.Generic;
using Shop.Domain.Model.ProductCategory;

namespace Shop.ObjectMothers
{
    public class ProductCategoryObjectMother
    {
        public static ProductCategoryModel CreateProductCategoryWithSubCategories()
        {
            var cat = new ProductCategoryModel
            {
                id = 1,
                name = "monitory",
                parentId = null
            };
            var sub1 = new ProductCategoryModel
            {
                id = 2,
                name = "LCD",
                parentId = cat,
                subCategory = null
            };
            var sub2 = new ProductCategoryModel
            {
                id = 3,
                name = "CRT",
                parentId = cat,
                subCategory = null
            };
            cat.subCategory = new HashedSet<ProductCategoryModel> {sub1, sub2};
            return cat;
        }
    }
}
