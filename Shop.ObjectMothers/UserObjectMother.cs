﻿using Shop.Domain.Model.User;
using Shop.Domain.Model.ValueObject;

namespace Shop.ObjectMothers
{
    public class UserObjectMother
    {
        public static UserModel CreateUser()
        {
            UserModel u = new UserModel
            {
                id = 1,
                username = "kowalski",
                adress = new Address { Address1 = "pierwszy" },
                password = "silnehaslo",
                discount = 0,
                userType = Type.customer
            };
            return u;
        }
    }
}
