﻿using System;
using System.Collections.Generic;
using System.Linq;
using Shop.Domain.Model.Producer;

namespace Shop.ObjectMothers
{
    public class ProducerObjectMother
    {
        public static List<ProducerModel> CreateProducerList()
        {
            List<ProducerModel> list = new List<ProducerModel>();

            list.Add(new ProducerModel
            {
                id = 1
            });
            list.Add(new ProducerModel
            {
                id = 2
            });
            list.Add(new ProducerModel
            {
                id = 3
            });
            list.Add(new ProducerModel
            {
                id = 4
            });
            list.Add(new ProducerModel
            {
                id = 5
            });
            return list;
        }
    }
}
