﻿using System.Collections.Generic;
using Shop.Domain.Model.Product;
using Shop.Domain.Model.ValueObject;

namespace Shop.ObjectMothers
{
    public class ProductObjectMother
    {
        public static ProductModel CreateProduct()
        {
            ProductModel product = new ProductModel
            {
                id = 1,
                name = "pierwszy",
                price = new Money { currency = Currency.pln, value = 10 },
                discount = 0,
                tax = new Domain.Model.Tax.TaxModel { id = 1, tax = 23 },
                desc = "",
                category = new Domain.Model.ProductCategory.ProductCategoryModel
                {
                    id = 1
                }
            };
            return product;
        }

        public static List<ProductModel> CreateProductsList()
        {
            List<ProductModel> list = new List<ProductModel>();
            list.Add(new ProductModel
            {
                id = 1,
                price = new Money { currency = Currency.pln, value = 10 },
                discount = 0,
                category = new Domain.Model.ProductCategory.ProductCategoryModel
                {
                    id = 1
                }
            });
            list.Add(new ProductModel
            {
                id = 2,
                price = new Money { currency = Currency.pln, value = 20 },
                discount = 0,
                category = new Domain.Model.ProductCategory.ProductCategoryModel
                {
                    id = 1
                }
            });
            list.Add(new ProductModel
            {
                id = 3,
                price = new Money { currency = Currency.pln, value = 30 },
                discount = 23,
                category = new Domain.Model.ProductCategory.ProductCategoryModel
                {
                    id = 1
                }
            });
            list.Add(new ProductModel
            {
                id = 1,
                price = new Money { currency = Currency.pln, value = 10 },
                discount = 10,
                category = new Domain.Model.ProductCategory.ProductCategoryModel
                {
                    id = 2
                }
            });
            return list;
        }
    }
}
