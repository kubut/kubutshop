﻿using System.Collections.Generic;
using Shop.Domain.Model.Order;
using Shop.Domain.Model.ValueObject;

namespace Shop.ObjectMothers
{
    public class OrderObjectMother
    {
        public static OrderModel CreateBasket()
        {
            return new OrderModel
            {
                id = 1,
                finish = 0,
                user = new Domain.Model.User.UserModel { id = 1 },
                productList = new List<OrderProductModel>(),
                status = Status.basket
            };
        }

        public static OrderModel CreateOrderOnWay()
        {
            return new OrderModel
            {
                id = 1,
                finish = 0,
                user = new Domain.Model.User.UserModel { id = 1 },
                productList = new List<OrderProductModel> {
                    new OrderProductModel{id = 1, price = new Money{ value = 10, currency = Currency.pln}},
                    new OrderProductModel{id = 2, price = new Money{ value = 10, currency = Currency.pln}},
                    new OrderProductModel{id = 3, price = new Money{ value = 10, currency = Currency.pln}}
                },
                status = Status.waiting
            };
        }

        public static OrderModel CreateFinishedOrder()
        {
            return new OrderModel
            {
                id = 1,
                finish = 1,
                user = new Domain.Model.User.UserModel { id = 1 },
                productList = new List<OrderProductModel> {
                    new OrderProductModel{id = 1, price = new Money{ value = 10, currency = Currency.pln}},
                    new OrderProductModel{id = 2, price = new Money{ value = 10, currency = Currency.pln}},
                    new OrderProductModel{id = 3, price = new Money{ value = 10, currency = Currency.pln}}
                },
                status = Status.send
            };
        }

        public static List<OrderModel> CreateOrderList()
        {
            return new List<OrderModel>{
                OrderObjectMother.CreateFinishedOrder(),
                OrderObjectMother.CreateFinishedOrder(),
                OrderObjectMother.CreateOrderOnWay(),
                OrderObjectMother.CreateOrderOnWay(),
                OrderObjectMother.CreateOrderOnWay(),
                OrderObjectMother.CreateOrderOnWay(),
            };
        }

        public static OrderProductModel CreateOrderProduct()
        {
            return new OrderProductModel
            {
                id = 1,
                name = "monitor"
            };
        }
    }
}
