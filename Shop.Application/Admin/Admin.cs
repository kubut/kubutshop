﻿using System.Collections.Generic;
using Shop.Infrastructure.Repositories;
using Shop.Domain.Model.User.Repositories;
using Shop.Domain.Model.Order.Repositories;
using Shop.Domain.Model.Product.Repositories;
using Shop.Domain.Model.ProductCategory.Repositories;
using Shop.Domain.Model.Producer.Repositories;

namespace Shop.Application.Admin
{
    public class AdminService : IAdmin
    {
        private IUserRepository userRepository;
        private IOrderRepository orderRepository;
        private IProductRepository productRepository;
        private IProducerRepository producerRepository;
        private IProductCategoryRepository productCategoryRepository;

        public AdminService()
        {
            userRepository              = new UserIM();
            orderRepository             = new OrderIM();
            productRepository           = new ProductIM();
            producerRepository          = new ProducerIM();
            productCategoryRepository   = new ProductCategoryIM();
        }

        public void setOrderRepository(IOrderRepository repo)
        {
            orderRepository = repo;
        }
        public void setUserRepository(IUserRepository repo)
        {
            userRepository = repo;
        }
        public void setProductRepository(IProductRepository repo)
        {
            productRepository = repo;
        }
        public void setProducerRepository(IProducerRepository repo)
        {
            producerRepository = repo;
        }
        public void setProductCategoryRepository(IProductCategoryRepository repo)
        {
            productCategoryRepository = repo;
        }


        public void deleteUser(int id)
        {
            this.userRepository.Delete(id);
        }
        public IList<Domain.Model.User.UserModel> GetUserList()
        {
            return this.userRepository.GetUserList();
        }
        public Domain.Model.User.UserModel getUserById(int id)
        {
            return this.userRepository.FindUserById(id);
        }


        public IList<Domain.Model.Product.ProductModel> GetProductList()
        {
            return this.productRepository.GetList();
        }
        public IList<Domain.Model.Product.ProductModel> GetProductListFromCategory(Domain.Model.ProductCategory.ProductCategoryModel cat)
        {
            return this.productRepository.GetListFromCategory(cat);
        }
        public Domain.Model.Product.ProductModel getProductById(int id)
        {
            return this.productRepository.GetProductById(id);
        }
        public void createNewProduct(Domain.Model.Product.ProductModel p)
        {
            this.productRepository.Insert(p);
        }
        public void updateProduct(Domain.Model.Product.ProductModel p)
        {
            this.productRepository.Update(p);
        }


        public void createNewProductCategory(Domain.Model.ProductCategory.ProductCategoryModel cat)
        {
            this.productCategoryRepository.Insert(cat);
        }
        public void deleteProductCategory(int id)
        {
            this.productCategoryRepository.Delete(id);
        }

        
        public IList<Domain.Model.Order.OrderModel> GetOrderList()
        {
            return this.orderRepository.SelectAllOrders();
        }
        public IList<Domain.Model.Order.OrderModel> GetOrderListByUser(int id)
        {
            return this.orderRepository.FindByUserId(id);
        }
        public IList<Domain.Model.Order.OrderModel> GetOrderInProgress()
        {
            return this.orderRepository.FindAllOnProgress();
        }
        public Domain.Model.Order.OrderModel getOrderById(int id)
        {
            return this.orderRepository.FindById(id);
        }
        public void changeOrderStatus(int id, Domain.Model.ValueObject.Status status)
        {
            Domain.Model.Order.OrderModel order = this.orderRepository.FindById(id);
            order.status = status;
            this.orderRepository.Update(order);
        }


        public void createNewProducer(Domain.Model.Producer.ProducerModel producer)
        {
            producerRepository.Insert(producer);
        }
    }
}
