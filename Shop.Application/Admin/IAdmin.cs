﻿using System.Collections.Generic;
using Shop.Domain.Model.User;
using Shop.Domain.Model.Product;
using Shop.Domain.Model.Producer;
using Shop.Domain.Model.Order;
using Shop.Domain.Model.ProductCategory;
using Shop.Domain.Model.ValueObject;
using Shop.Domain.Model.Order.Repositories;
using Shop.Domain.Model.Producer.Repositories;
using Shop.Domain.Model.User.Repositories;
using Shop.Domain.Model.Product.Repositories;
using Shop.Domain.Model.ProductCategory.Repositories;

namespace Shop.Application
{
    public interface IAdmin
    {
        void setOrderRepository(IOrderRepository repo);
        void setUserRepository(IUserRepository repo);
        void setProductRepository(IProductRepository repo);
        void setProducerRepository(IProducerRepository repo);
        void setProductCategoryRepository(IProductCategoryRepository repo);

        void deleteUser(int id);
        IList<UserModel> GetUserList();
        UserModel getUserById(int id);

        IList<ProductModel> GetProductList();
        IList<ProductModel> GetProductListFromCategory(ProductCategoryModel cat);
        ProductModel getProductById(int id);
        void createNewProduct(ProductModel p);
        void createNewProductCategory(ProductCategoryModel cat);
        void deleteProductCategory(int id);
        void updateProduct(ProductModel p);

        IList<OrderModel> GetOrderList();
        IList<OrderModel> GetOrderListByUser(int id);
        IList<OrderModel> GetOrderInProgress();
        OrderModel getOrderById(int id);
        void changeOrderStatus(int id, Status status);

        void createNewProducer(ProducerModel producer);
    }
}
