﻿using System.Collections.Generic;
using Shop.Domain.Model.Order;
using Shop.Domain.Model.Product;
using Shop.Domain.Model.ProductCategory;
using Shop.Domain.Model.User;
using Shop.Domain.Model.ValueObject;
using Shop.Domain.Model.Order.Repositories;
using Shop.Domain.Model.User.Repositories;
using Shop.Domain.Model.Product.Repositories;
using Shop.Domain.Model.ProductCategory.Repositories;

namespace Shop.Application
{
    public interface IFront
    {
        void setOrderRepository(IOrderRepository repo);
        void setUserRepository(IUserRepository repo);
        void setProductRepository(IProductRepository repo);
        void setProductCategoryRepository(IProductCategoryRepository repo);

        void createNewOrder(OrderModel p);
        IList<OrderModel> GetOrderListByUser(int id);

        UserModel getUserById(int id);
        string registerNewUser(UserModel u);
        bool login(string name, string pass);
        void updateUser(UserModel u);
        void updateUserAdress(Address a, int userId);
        void changePassword(string pass, int id);


        IList<ProductCategoryModel> GetProductCategoryList();
        ProductCategoryModel GetProductCategoryById(int id);
        IList<ProductCategoryModel> GetSubcategoryList(ProductCategoryModel cat);

        IList<ProductModel> GetProductList();
        IList<ProductModel> GetProductsWithDiscount();
        IList<ProductModel> GetProductListFromCategory(ProductCategoryModel cat);

        void addToBasket(int id, OrderProductModel p);
    }
}
