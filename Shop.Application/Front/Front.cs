﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Shop.Infrastructure.Repositories;
using Shop.Domain.Model.User.Repositories;
using Shop.Domain.Model.Order.Repositories;
using Shop.Domain.Model.Product.Repositories;
using Shop.Domain.Model.ProductCategory.Repositories;
using Shop.Domain.Model.ValueObject;

namespace Shop.Application.Front
{
    public class FrontService : IFront
    {
        public IUserRepository userRepository;
        public IOrderRepository orderRepository;
        public IProductRepository productRepository;
        public IProductCategoryRepository productCategoryRepository;

        public FrontService()
        {
            userRepository              = new UserIM();
            orderRepository             = new OrderIM();
            productRepository           = new ProductIM();
            productCategoryRepository   = new ProductCategoryIM();
        }

        public void setOrderRepository(IOrderRepository repo)
        {
            orderRepository = repo;
        }
        public void setUserRepository(IUserRepository repo)
        {
            userRepository = repo;
        }
        public void setProductRepository(IProductRepository repo)
        {
            productRepository = repo;
        }
        public void setProductCategoryRepository(IProductCategoryRepository repo)
        {
            productCategoryRepository = repo;
        }

        public void createNewOrder(Domain.Model.Order.OrderModel p)
        {
            this.orderRepository.Insert(p);
        }
        public IList<Domain.Model.Order.OrderModel> GetOrderListByUser(int id)
        {
            return this.orderRepository.FindByUserId(id);
        }

        public Domain.Model.User.UserModel getUserById(int id)
        {
            return this.userRepository.FindUserById(id);
        }
        public string registerNewUser(Domain.Model.User.UserModel u)
        {
            if (this.userRepository.FindUserByName(u.username) != null)
            {
                return "Nazwa użytkownika zajęta";
            }
            this.userRepository.Insert(u);
            return "Zarejestrowano";
        }
        public bool login(string name, string pass)
        {
            Domain.Model.User.UserModel user = this.userRepository.FindUserByName(name);
            if (user == null)
            {
                return false;
            }

            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] hashValue;
            byte[] message = UE.GetBytes(pass);

            SHA256Managed hashString = new SHA256Managed();
            string hex = "";

            hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }

            return user.password == hex;
        }
        public void updateUser(Domain.Model.User.UserModel u)
        {
            this.userRepository.Update(u);
        }
        public void changePassword(string pass, int id)
        {
            Domain.Model.User.UserModel user = this.userRepository.FindUserById(id);
            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] hashValue;
            byte[] message = UE.GetBytes(pass);

            SHA256Managed hashString = new SHA256Managed();
            string hex = "";

            hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }

            user.password = hex;

            this.userRepository.Update(user);
        }

        public IList<Domain.Model.Product.ProductModel> GetProductList()
        {
            return this.productRepository.GetList();
        }
        public IList<Domain.Model.Product.ProductModel> GetProductListFromCategory(Domain.Model.ProductCategory.ProductCategoryModel cat)
        {
            return this.productRepository.GetListFromCategory(cat);
        }
        public void addToBasket(int id, Domain.Model.Order.OrderProductModel p)
        {
           
            /*Domain.Model.Order.OrderModel basket = this.orderRepository.FindBasketByUserId(id);
            basket.productList.Add(p);
            this.orderRepository.Update(basket);*/
        }
        public void updateUserAdress(Domain.Model.ValueObject.Address a, int userId)
        {
            Domain.Model.User.UserModel user = this.userRepository.FindUserById(userId);
            user.adress = a;
            this.userRepository.Update(user);
        }

        public Domain.Model.ProductCategory.ProductCategoryModel GetProductCategoryById(int id)
        {
            return this.productCategoryRepository.GetCategoryById(id);
        }
        public IList<Domain.Model.ProductCategory.ProductCategoryModel> GetProductCategoryList()
        {
            return this.productCategoryRepository.GetList();
        }
        public IList<Domain.Model.ProductCategory.ProductCategoryModel> GetSubcategoryList(Domain.Model.ProductCategory.ProductCategoryModel cat)
        {
            return this.productCategoryRepository.GetSubList(cat);
        }
        public IList<Domain.Model.Product.ProductModel> GetProductsWithDiscount()
        {
            List<Domain.Model.Product.ProductModel> list = new List<Domain.Model.Product.ProductModel>();
            foreach (Domain.Model.Product.ProductModel product in this.productRepository.GetList())
            {
                if (product.discount > 0)
                {
                    list.Add(product);
                }
            }
            return list;
        }
    }
}
