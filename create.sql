﻿CREATE TABLE [dbo].[TaxModel] (
    [id]  INT IDENTITY (1, 1) NOT NULL,
    [tax] INT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);
go

create table ProductCategoryModel
(
	id int primary key identity,
	name varchar(50),
	parentId int references ProductCategoryModel(id)
)
go

CREATE TABLE [dbo].[ProducerModel] (
    [id]   INT          IDENTITY (1, 1) NOT NULL,
    [name] VARCHAR (40) NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);
go

create table UserModel
(
	id int primary key identity,
	username varchar(40),
	FirstName varchar(100),
	SecondName varchar(100),
	Address1 varchar(100),
	Address2 varchar(100),
	Phone varchar(15),
	password varchar(64),
	discount int,
	userType varchar(30),
)
go

create table ProductModel (
	id int primary key identity,
	name varchar(60),
	value int,
	currency varchar(5),
	discount int,
	tax int references TaxModel(id),
	descript text,
	avaible int,
	date date,
	producer int references ProducerModel(id),
	category int references ProductCategoryModel(id)
)
go

create table OrderModel (
	id int primary key identity,
	finish int,
	userId int references UserModel(id),
	FirstName varchar(100),
	SecondName varchar(100),
	Address1 varchar(100),
	Address2 varchar(100),
	Phone varchar(15),
	paymentMethod varchar(15),
	paymentStatus varchar(15),
	paymentDate date
)
go

create table OrderProductModel (
	id int primary key identity,
	name varchar(60),
	value int,
	currency varchar(5),
	countNumber int,
	tax int references TaxModel(id),
    orderM int references OrderModel(id)
)
go
