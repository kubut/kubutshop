﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shop.Domain.Model.Order.Repositories;
using Shop.Domain.Model.User.Repositories;
using Shop.Domain.Model.Product.Repositories;
using Shop.Domain.Model.ProductCategory.Repositories;
using Shop.Domain.Model.ProductCategory;
using Shop.Domain.Model.User;
using Shop.Domain.Model.Order;
using Shop.Domain.Model.ValueObject;
using Shop.Domain.Model.Product;
using Shop.Application.Front;
using Shop.ObjectMothers;
using Moq;

namespace Shop.Application.UnitTests
{
    [TestClass]
    public class FrontServiceTests
    {
        [TestMethod]
        public void CheckCreateNewOrder()
        {
            Mock<IOrderRepository> repositoryMock = new Mock<IOrderRepository>();
            IFront bs = new FrontService();

            bs.setOrderRepository(repositoryMock.Object);

            bs.createNewOrder(null);

            repositoryMock.Verify(k => k.Insert(null), Times.Once());
        }

        [TestMethod]
        public void CheckGetOrderListByUser()
        {
            Mock<IOrderRepository> repositoryMock = new Mock<IOrderRepository>();
            IFront bs = new FrontService();

            bs.setOrderRepository(repositoryMock.Object);

            bs.GetOrderListByUser(1);

            repositoryMock.Verify(k => k.FindByUserId(1), Times.Once());
        }

        [TestMethod]
        public void CheckGetUserById()
        {
            Mock<IUserRepository> repositoryMock = new Mock<IUserRepository>();
            IFront bs = new FrontService();

            bs.setUserRepository(repositoryMock.Object);

            bs.getUserById(1);

            repositoryMock.Verify(k => k.FindUserById(1), Times.Once());
        }

        [TestMethod]
        public void CheckRegisterNewUser()
        {
            Mock<IUserRepository> repositoryMock = new Mock<IUserRepository>();
            IFront bs = new FrontService();

            bs.setUserRepository(repositoryMock.Object);

            UserModel user = UserObjectMother.CreateUser();

            bs.registerNewUser(user);

            repositoryMock.Verify(k => k.Insert(user), Times.Once());
        }

        [TestMethod]
        public void CheckUpdateUser()
        {
            Mock<IUserRepository> repositoryMock = new Mock<IUserRepository>();
            IFront bs = new FrontService();

            bs.setUserRepository(repositoryMock.Object);

            bs.updateUser(null);

            repositoryMock.Verify(k => k.Update(null), Times.Once());
        }

        [TestMethod]
        public void CheckChangePassword()
        {
            Mock<IUserRepository> repositoryMock = new Mock<IUserRepository>();
            IFront bs = new FrontService();

            bs.setUserRepository(repositoryMock.Object);

            UserModel user = UserObjectMother.CreateUser();
            repositoryMock.Setup(k => k.FindUserById(1)).Returns(user);
            bs.changePassword("haslo",1);

            repositoryMock.Verify(c => c.Update(It.IsAny<UserModel>()), Times.Once());
            Assert.AreEqual(user.password, "7619b8d1b1dc6f04e60d99e9db71eb2b87feb919b2abfdcbabce41f5f2ac51cc");
        }

        [TestMethod]
        public void CheckLogin()
        {
            Mock<IUserRepository> repositoryMock = new Mock<IUserRepository>();
            IFront bs = new FrontService();

            bs.setUserRepository(repositoryMock.Object);

            UserModel user = null;

            repositoryMock.Setup(k => k.FindUserByName("Nieistniejący")).Returns(user);

            Assert.AreEqual(bs.login("Nieistniejący", "pass"), false);
        }

        [TestMethod]
        public void CheckGetProductList()
        {
            Mock<IProductRepository> repositoryMock = new Mock<IProductRepository>();
            IFront bs = new FrontService();

            bs.setProductRepository(repositoryMock.Object);

            bs.GetProductList();

            repositoryMock.Verify(k => k.GetList(), Times.Once());
        }

        [TestMethod]
        public void CheckGetProductListFromCategory()
        {
            Mock<IProductRepository> repositoryMock = new Mock<IProductRepository>();
            IFront bs = new FrontService();

            bs.setProductRepository(repositoryMock.Object);

            ProductCategoryModel cat = ProductCategoryObjectMother.CreateProductCategoryWithSubCategories();

            bs.GetProductListFromCategory(cat);

            repositoryMock.Verify(k => k.GetListFromCategory(cat), Times.Once());
        }

        /*[TestMethod]
        public void CheckAddToBasket()
        {
            Mock<IOrderRepository> repositoryMock = new Mock<IOrderRepository>();
            IFront bs = new FrontService();

            bs.setOrderRepository(repositoryMock.Object);

            OrderModel basket = OrderObjectMother.CreateBasket();
            OrderProductModel product = OrderObjectMother.CreateOrderProduct();

            repositoryMock.Setup(k => k.FindBasketByUserId(1)).Returns(basket);

            bs.addToBasket(1,product);

            Assert.AreEqual(basket.productList.Count, 1);
        }*/

        [TestMethod]
        public void CheckUpdateUserAdress()
        {
            Mock<IUserRepository> repositoryMock = new Mock<IUserRepository>();
            IFront bs = new FrontService();

            UserModel user = UserObjectMother.CreateUser();
            repositoryMock.Setup(k => k.FindUserById(1)).Returns(user);

            bs.setUserRepository(repositoryMock.Object);

            bs.updateUserAdress(new Address { FirstName="Zbyszek" }, 1);

            Assert.AreEqual(user.adress.FirstName, "Zbyszek");
        }

        [TestMethod]
        public void CheckGetProductCategoryList()
        {
            Mock<IProductCategoryRepository> repositoryMock = new Mock<IProductCategoryRepository>();
            IFront bs = new FrontService();

            bs.setProductCategoryRepository(repositoryMock.Object);

            bs.GetProductCategoryList();

            repositoryMock.Verify(k => k.GetList(), Times.Once());
        }

        [TestMethod]
        public void CheckGetSubcategoryList()
        {
            Mock<IProductCategoryRepository> repositoryMock = new Mock<IProductCategoryRepository>();
            IFront bs = new FrontService();

            bs.setProductCategoryRepository(repositoryMock.Object);

            ProductCategoryModel cat = ProductCategoryObjectMother.CreateProductCategoryWithSubCategories();
            bs.GetSubcategoryList(cat);

            repositoryMock.Verify(k => k.GetSubList(cat), Times.Once());
        }

        [TestMethod]
        public void CheckGetProductsWithDiscount()
        {
            Mock<IProductRepository> repositoryMock = new Mock<IProductRepository>();
            IFront bs = new FrontService();

            bs.setProductRepository(repositoryMock.Object);
            
            List<ProductModel> list = ProductObjectMother.CreateProductsList();
            repositoryMock.Setup(k => k.GetList()).Returns(list);

            Assert.AreEqual(bs.GetProductsWithDiscount().Count, 2);
        }
    }
}
