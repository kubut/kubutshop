﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shop.Domain.Model.Order.Repositories;
using Shop.Domain.Model.User.Repositories;
using Shop.Domain.Model.Product.Repositories;
using Shop.Domain.Model.ProductCategory.Repositories;
using Shop.Domain.Model.ProductCategory;
using Shop.Domain.Model.Order;
using Shop.Domain.Model.ValueObject;
using Shop.Domain.Model.Product;
using Shop.Application.Admin;
using Shop.ObjectMothers;
using Moq;

namespace Shop.Application.UnitTests
{
    [TestClass]
    public class AdminServiceTests
    {
        [TestMethod]
        public void CheckDeleteUser()
        {
            Mock<IUserRepository> repositoryMock = new Mock<IUserRepository>();
            IAdmin bs = new AdminService();

            bs.setUserRepository(repositoryMock.Object);

            bs.deleteUser(1);

            repositoryMock.Verify(k => k.Delete(1), Times.Once());
        }

        [TestMethod]
        public void CheckGetUserList()
        {
            Mock<IUserRepository> repositoryMock = new Mock<IUserRepository>();
            IAdmin bs = new AdminService();

            bs.setUserRepository(repositoryMock.Object);

            bs.GetUserList();

            repositoryMock.Verify(k => k.GetUserList(), Times.Once());
        }

        [TestMethod]
        public void CheckGetUserById()
        {
            Mock<IUserRepository> repositoryMock = new Mock<IUserRepository>();
            IAdmin bs = new AdminService();

            bs.setUserRepository(repositoryMock.Object);

            bs.getUserById(1);

            repositoryMock.Verify(k => k.FindUserById(1), Times.Once());
        }

        [TestMethod]
        public void CheckGetProductList()
        {
            Mock<IProductRepository> repositoryMock = new Mock<IProductRepository>();
            IAdmin bs = new AdminService();

            bs.setProductRepository(repositoryMock.Object);

            bs.GetProductList();

            repositoryMock.Verify(k => k.GetList(), Times.Once());
        }

        [TestMethod]
        public void CheckGetProductListFromCategory()
        {
            Mock<IProductRepository> repositoryMock = new Mock<IProductRepository>();
            IAdmin bs = new AdminService();

            bs.setProductRepository(repositoryMock.Object);
            ProductCategoryModel cat = ProductCategoryObjectMother.CreateProductCategoryWithSubCategories();
            bs.GetProductListFromCategory(cat);

            repositoryMock.Verify(k => k.GetListFromCategory(cat), Times.Once());
        }

        [TestMethod]
        public void CheckGetProductById()
        {
            Mock<IProductRepository> repositoryMock = new Mock<IProductRepository>();
            IAdmin bs = new AdminService();

            bs.setProductRepository(repositoryMock.Object);

            bs.getProductById(1);

            repositoryMock.Verify(k => k.GetProductById(1), Times.Once());
        }

        [TestMethod]
        public void CheckCreateNewProduct()
        {
            Mock<IProductRepository> repositoryMock = new Mock<IProductRepository>();
            IAdmin bs = new AdminService();

            bs.setProductRepository(repositoryMock.Object);
            ProductModel product = ProductObjectMother.CreateProduct();

            bs.createNewProduct(product);

            repositoryMock.Verify(k => k.Insert(product), Times.Once());
        }

        [TestMethod]
        public void CheckUpdateProduct()
        {
            Mock<IProductRepository> repositoryMock = new Mock<IProductRepository>();
            IAdmin bs = new AdminService();

            bs.setProductRepository(repositoryMock.Object);
            ProductModel product = ProductObjectMother.CreateProduct();

            bs.updateProduct(product);

            repositoryMock.Verify(k => k.Update(product), Times.Once());
        }

        [TestMethod]
        public void CheckCreateNewProductCategory()
        {
            Mock<IProductCategoryRepository> repositoryMock = new Mock<IProductCategoryRepository>();
            IAdmin bs = new AdminService();

            bs.setProductCategoryRepository(repositoryMock.Object);
            ProductCategoryModel cat = ProductCategoryObjectMother.CreateProductCategoryWithSubCategories();

            bs.createNewProductCategory(cat);

            repositoryMock.Verify(k => k.Insert(cat), Times.Once());
        }

        [TestMethod]
        public void CheckDeleteProductCategory()
        {
            Mock<IProductCategoryRepository> repositoryMock = new Mock<IProductCategoryRepository>();
            IAdmin bs = new AdminService();

            bs.setProductCategoryRepository(repositoryMock.Object);

            bs.deleteProductCategory(1);

            repositoryMock.Verify(k => k.Delete(1), Times.Once());
        }

        [TestMethod]
        public void CheckGetOrderList()
        {
            Mock<IOrderRepository> repositoryMock = new Mock<IOrderRepository>();
            IAdmin bs = new AdminService();

            bs.setOrderRepository(repositoryMock.Object);

            bs.GetOrderList();

            repositoryMock.Verify(k => k.SelectAllOrders(), Times.Once());
        }

        [TestMethod]
        public void CheckGetOrderListByUser()
        {
            Mock<IOrderRepository> repositoryMock = new Mock<IOrderRepository>();
            IAdmin bs = new AdminService();

            bs.setOrderRepository(repositoryMock.Object);

            bs.GetOrderListByUser(1);

            repositoryMock.Verify(k => k.FindByUserId(1), Times.Once());
        }

        [TestMethod]
        public void CheckGetOrderInProgress()
        {
            Mock<IOrderRepository> repositoryMock = new Mock<IOrderRepository>();
            IAdmin bs = new AdminService();

            bs.setOrderRepository(repositoryMock.Object);

            bs.GetOrderInProgress();

            repositoryMock.Verify(k => k.FindAllOnProgress(), Times.Once());
        }

        [TestMethod]
        public void CheckGetOrderById()
        {
            Mock<IOrderRepository> repositoryMock = new Mock<IOrderRepository>();
            IAdmin bs = new AdminService();

            bs.setOrderRepository(repositoryMock.Object);

            bs.getOrderById(1);

            repositoryMock.Verify(k => k.FindById(1), Times.Once());
        }

        [TestMethod]
        public void CheckChangeOrderStatus()
        {
            Mock<IOrderRepository> repositoryMock = new Mock<IOrderRepository>();
            IAdmin bs = new AdminService();

            bs.setOrderRepository(repositoryMock.Object);
            OrderModel order = OrderObjectMother.CreateOrderOnWay();
            repositoryMock.Setup(k => k.FindById(1)).Returns(order);

            bs.changeOrderStatus(1, Status.send);

            Assert.AreEqual(order.status,Status.send);
        }

    }
}
