﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NHibernate;
using NHibernate.Cfg;
using Shop.Application;
using Shop.Application.Front;
using Shop.Domain.Model.Order;
using Shop.Domain.Model.ProductCategory;
using Shop.Infrastructure.Repositories;

namespace NhibernateUnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        [ExpectedException(typeof(LazyInitializationException))]
        public void LazyLoadingCategories()
        {
            Configuration cfg = new Configuration();
            cfg.AddAssembly("Shop.Domain");
            cfg.Configure();
            ProductCategoryModel cat;
            using (ISession session = cfg.BuildSessionFactory().OpenSession())
            {
                cat = session.Load<ProductCategoryModel>(104);
            }
            var t = cat.subCategory;
        }

        [TestMethod]
        public void EagerLoadingOrder()
        {
            Configuration cfg = new Configuration();
            cfg.AddAssembly("Shop.Domain");
            cfg.Configure();
            OrderModel order;
            using (ISession session = cfg.BuildSessionFactory().OpenSession())
            {
                order = session.Get<OrderModel>(75);
            }
            Assert.IsTrue(NHibernateUtil.IsInitialized(order.productList));
        }
    }
}
